<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Course;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    //login users
    public function login(Request $request){
        $validation = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required',
        ]);

        if($validation->fails()){
            return response()->json($validator->errors()->toJson());
        }

        $login_details = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($login_details)) {
                return response()->json(['status'=>'failed', 'message' => 'Invalid Login Details']);
            }
        } catch (JWTException $e) {
            return response()->json(['status'=>'failed', 'message' => 'Could not create Token']);
        }

        return response()->json(compact('token'));

    }

    //register users
    public function register(Request $request){
        
        $validation = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if($validation->fails()){
            return response()->json($validation->errors()->toJson());
        }

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user','token'));


    }

    public function course_register(Request $request){
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['User not Found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                return response()->json(['Token Expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                return response()->json(['Invalid TOken'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                return response()->json(['Token is required'], $e->getStatusCode());

        }
      
        $user = json_decode(json_encode(compact('user')));
        $user_id = $user->user->id;
        $courses = $request->course_id;

        if(is_array($courses) && !empty($courses)):
            foreach($courses as $course):
                $data_to_insert = [
                    'user_id' => $user_id,
                    'course_id' => $course,
                ];
                // Course::insert($data_to_insert);
                DB::table('course_registers')->insert($data_to_insert);
            endforeach;
            return response()->json(['status'=>'success', 'message'=>'Course register Success']);
        else:
            return response()->json(['status'=>'error', 'message'=>'CourseID must be an array']);
        endif;

        // return response()->json(compact('user'));
    }

    public function get_courses(){
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['User not Found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                return response()->json(['Token Expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                return response()->json(['Invalid TOken'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                return response()->json(['Token is required'], $e->getStatusCode());

        }

        $user = json_decode(json_encode(compact('user')));
        $user_id = $user->user->id;
        $date = 
        $course_registered = DB::table('course_registers')->select('course_id', DB::raw('DATE(`created_at`)'))->where('user_id', $user_id)->get();
        $all_courses = DB::table('courses')->get();
        if(!empty($course_registered)){
            return json_encode(['registered_courses' => $course_registered, 'all_courses' => $all_courses]);
        }else{
            return json_encode(['registered_courses' => 'You have no enrolled course', 'all_courses' => $all_courses]);
        }
    }
}
