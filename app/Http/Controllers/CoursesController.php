<?php

namespace App\Http\Controllers;
use App\Jobs\CreateCourses;
use App\User;
use App\Course;
use Illuminate\Http\Request;
use App\Exports\CourseExport;
use Maatwebsite\Excel\Facades\Excel;

class CoursesController extends Controller
{
    //
    public function create(){
        // \Artisan::call('db:seed', array('--class' => 'CourseTableSeeder'));
        CreateCourses::dispatch()->delay(now()->addSeconds(5));
        return json_encode(['status' => 'suucces', 'message' => '50 entries created']);
    }

    public function get(){
        return Course::all();
    }

    public function export(){
        return Excel::download(new CourseExport, 'course.xlsx');
    }
}
