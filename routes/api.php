<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


    Route::post('login', 'UserController@login');
    Route::post('register', 'UserController@register');
    Route::get('create-courses','CoursesController@create');
    Route::get('export-courses','CoursesController@export');
    Route::group(['middleware' => ['jwt.verify']], function() {
        Route::get('get-courses','UserController@get_courses');
        Route::post('course-register', 'UserController@course_register');
    });



