<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Course;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/ 

$factory->define(Course::class, function (Faker $faker) {
    return [
        'text' => $faker->sentence(5),
        'course_id' => 'course_id'.$faker->numberBetween($min = 109090, $max = 2000000),
        'course_title' => 'COURSE '.$faker->numberBetween($min = 100, $max = 200),
        'course_desc' => $faker->text(), 
    ];
});
